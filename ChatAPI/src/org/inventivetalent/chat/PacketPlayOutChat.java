/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.chat;

import net.minecraft.server.v1_7_R4.*;
import org.inventivetalent.reflection.util.AccessUtil;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;

public class PacketPlayOutChat extends Packet {

	private IChatBaseComponent a;
	private boolean            b;
	private int                pos;

	public PacketPlayOutChat() {
		b = true;
	}

	public PacketPlayOutChat(IChatBaseComponent ichatbasecomponent) {
		this(ichatbasecomponent, true);
	}

	public PacketPlayOutChat(IChatBaseComponent ichatbasecomponent, int pos) {
		this(ichatbasecomponent, pos, true);
	}

	public PacketPlayOutChat(IChatBaseComponent ichatbasecomponent, boolean flag) {
		this(ichatbasecomponent, 0, flag);
	}

	public PacketPlayOutChat(IChatBaseComponent ichatbasecomponent, int pos, boolean flag) {
		b = true;
		a = ichatbasecomponent;
		b = flag;
		this.pos = pos;
	}

	public void a(PacketDataSerializer packetdataserializer) throws IOException {
		a = ChatSerializer.a(packetdataserializer.c(32767));
	}

	public void b(PacketDataSerializer packetdataserializer) throws IOException {
		packetdataserializer.a(ChatSerializer.a(a));
		int version = 0;
		try {
			version = AccessUtil.setAccessible(Reflection.getField(packetdataserializer.getClass(), "version")).getInt(packetdataserializer);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (version >= 16) { packetdataserializer.writeByte(pos); }
	}

	public void a(PacketPlayOutListener packetplayoutlistener) {
	}

	public String b() {
		return String.format("message='%s'", new Object[] { a });
	}

	public boolean d() {
		return b;
	}

	public void handle(PacketListener packetlistener) {
		a((PacketPlayOutListener) packetlistener);
	}

	protected static void register() {
		registerPacket(EnumProtocol.PLAY, PacketPlayOutChat.class, 2);
		System.out.println("[ChatAPI] Injected custom chat packet");
	}

	private static void registerPacket(EnumProtocol protocol, Class<? extends Packet> packetClass, int packetID) {
		try {
			protocol.b().put(packetID, packetClass);
			Field mapField = EnumProtocol.class.getDeclaredField("f");
			mapField.setAccessible(true);
			Map<Class<? extends Packet>, EnumProtocol> map = (Map<Class<? extends Packet>, EnumProtocol>) mapField.get(null);
			map.put(packetClass, protocol);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
