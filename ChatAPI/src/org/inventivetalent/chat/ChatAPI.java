/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.chat;

import org.bukkit.entity.Player;

/**
 *
 * Class to send JSON/Tellraw messages
 *
 * @author inventivetalent
 *
 */
public abstract class ChatAPI {

	/* Position values */

	/**
	 * Chat message
	 */
	public static final int	CHAT		= 0;

	/**
	 * System message
	 */
	public static final int	SYSTEM		= 1;

	/**
	 * Action bar message
	 */
	public static final int	ACTION_BAR	= 2;

	/* Send methods */

	/**
	 * Sends a JSON/Tellraw message to a player
	 *
	 * @param player
	 *            Receiver of the message
	 * @param message
	 *            Message to send
	 * @param position
	 *            Position of the message ({@link #CHAT}, {@link #SYSTEM}, {@link #ACTION_BAR})
	 */
	public static void sendRawMessage(Player player, String message, int position, boolean system) {
		if (player == null) throw new IllegalArgumentException("player cannot be null");
		if (message == null) throw new IllegalArgumentException("message cannot be null");
		if (!message.startsWith("{") || !message.endsWith("}")) throw new IllegalArgumentException("message is not a JSON string");
		Util.sendRawMessage(player, message, position, system);
	}

	/**
	 * Sends a JSON/Tellraw message to a player (system message = true)
	 *
	 * @param player
	 *            Receiver of the message
	 * @param message
	 *            Message to send
	 * @param position
	 *            Position of the message ({@link #CHAT}, {@link #SYSTEM}, {@link #ACTION_BAR})
	 */
	public static void sendRawMessage(Player player, String message, int position) {
		sendRawMessage(player, message, position, true);
	}

	/**
	 * Sends a JSON/Tellraw message to a player (system message = true | position = 0)
	 *
	 * @param player
	 *            Receiver of the message
	 * @param message
	 *            Message to send
	 */
	public static void sendRawMessage(Player player, String message) {
		sendRawMessage(player, message, 0);
	}

	/**
	 * Creates a JSON message from a string
	 */
	public static String toJson(String str) {
		if (str.startsWith("{") && str.endsWith("}")) return str;
		return "{\"text\":\"" + str + "\"}";
	}

	protected ChatAPI() {
	}

}
