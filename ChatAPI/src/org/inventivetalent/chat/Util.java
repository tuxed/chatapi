/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.chat;

import org.bukkit.entity.Player;
import org.inventivetalent.reflection.minecraft.Minecraft;
import org.inventivetalent.reflection.resolver.minecraft.NMSClassResolver;

class Util {

	public static boolean replacePacket = false;

	protected static void sendRawMessage(Player player, String message, int position, boolean flag) {
		try {
			Object handle = Reflection.getHandle(player);
			Object connection = Reflection.getField(handle.getClass(), "playerConnection").get(handle);
			Object serialized = message.isEmpty() ? EMPTY_COMPONENT : Reflection.getMethod(nmsChatSerializer, "a", new Class[] { String.class }).invoke(null, new Object[] { message });
			Object packet = null;
			if (Minecraft.VERSION.olderThan(Minecraft.Version.v1_8_R1)) {
				packet = nmsPacketPlayOutChat.getConstructor(new Class[] {
						nmsIChatBaseComponent,
						int.class,
						boolean.class }).newInstance(new Object[] {
						serialized,
						position,
						flag });
			} else {
				packet = nmsPacketPlayOutChat.getConstructor(new Class[] {
						nmsIChatBaseComponent,
						byte.class }).newInstance(new Object[] {
						serialized,
						(byte) position });
			}
			if (packet != null) {
				Reflection.getMethod(connection.getClass(), "sendPacket", new Class[0]).invoke(connection, new Object[] { packet });
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static NMSClassResolver nmsClassResolver = new NMSClassResolver();
	private static Class<?> nmsIChatBaseComponent;
	private static Class<?> nmsChatSerializer;
	private static Class<?> nmsPacketPlayOutChat;

	protected static Object EMPTY_COMPONENT;

	static {
		if (replacePacket && Minecraft.VERSION == Minecraft.Version.v1_7_R4) {
			PacketPlayOutChat.register();
		}
		try {
			nmsIChatBaseComponent = Reflection.getNMSClass("IChatBaseComponent");
			nmsChatSerializer = nmsClassResolver.resolve("hatSerializer", "IChatBaseComponent$ChatSerializer");
			nmsPacketPlayOutChat = replacePacket && Minecraft.VERSION == Minecraft.Version.v1_7_R4 ? PacketPlayOutChat.class : Reflection.getNMSClass("PacketPlayOutChat");
			EMPTY_COMPONENT = Reflection.getNMSClass("ChatComponentText").getConstructor(String.class).newInstance("");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
